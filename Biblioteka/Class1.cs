﻿using System;

namespace Biblioteka
{
    public class Formula
    {
        public static double B { get; set; }
        public static double M { get; set; }
        public static double GM { get; set; }
        public static double QNRM { get; set; }
        public static double QNRG { get; set; }
        public static double ALFA { get; set; }
        public static double KP { get; set; }
        public static double VK { get; set; }
        public static double VKB { get; set; }
        public static double VBB { get; set; }
        public static double NR { get; set; }
        public static double VKV { get; set; }
        public static double N2K { get; set; }
        public static double FG { get; set; }
        public static double O2V { get; set; }
        public static double VALFAV { get; set; }
        public static double TBV { get; set; }
        public static double TV { get; set; }
        public static double O2IV { get; set; }
        public static double N2V { get; set; }
        public static double NORV { get; set; }
        public static double NO1V { get; set; }
        public static double VNOV { get; set; }
        public static double MCNOV { get; set; }
        public static double MYNOV { get; set; }
        public static double MNOV { get; set; }
        public static double NOV { get; set; }

        public static double VNO (double B, double M, double GM, double QNRM, double NR, double VKV, double N2k, double NO1, double VALFA)
        {
            return Math.Round(((0.01 * VALFA * NO1) + (((2.1422 * 0.01 * NR * B * M * GM) / (QNRM / 29310)) / 1.3402) + (0.02142 * VKV * N2k)), 3); ;
        }
        public static double MNO(double B, double M, double GM, double QNRM, double NR, double VKV, double N2K, double NO1, double VALFA)
        {
            return Math.Round(((0.01*VALFA*NO1*1.3402)+((2.1422*0.01*NR*B*M*GM)/(QNRM/29310))+(0.021422*VKV*N2K*1.205)),3);
        }
        public static double MYNO(double B, double M, double GM, double QNRM, double NR, double VKV, double N2K, double NO1, double VALFA)
        {
            return Math.Round((0.01 * VALFA * NO1 * 1.3402 / M) + (2.1422 * 0.01 * NR * B * M * GM / (QNRM / 29310) / M) + ((0.021422 * VKV * N2K * 1.2505) / M), 3);
        }
        public static double MCNO(double B, double M, double GM, double QNRM, double NR, double VKV, double N2K, double NO1, double VALFA)
        {
            return Math.Round((0.01 * NO1* 1.3402) + (2.1422 * (0.01 * NR * B * M * GM / (QNRM / 29310)) / (3600 * VALFA)) + (0.021422 * VKV * N2K * 1.2505 / (3600 * VALFA)), 6);
        }
        public static double NOR(double KP, double N2, double O2I, double TB)
        {
            return Math.Round(Math.Sqrt(N2 * O2I) * Math.Exp(-21500 / (TB * KP)), 5);
        }
        public static double NO1 (double KP, double NOR, double TB, double T)
        {
            return Math.Round(NOR * (1 + Math.Pow(Math.Sqrt(Math.Pow(T - (TB * KP), 2)) / (TB * KP), 2) / 2 * (0.5 * (Math.Pow(Math.Exp(-67500 / T), 2) - Math.Pow(Math.Exp(-46000 / T), 2)) + (Math.Exp(-67500 / T) - Math.Exp(-46000 / T)))), 5);
        }
        public static double N2(double O2, double ALFA, double GM)
        {
            return Math.Round(84.943 - (0.9159 * O2) + (3.2501 * ALFA) + (2.0977 * GM), 2);
        }
        public static double O2I (double GM, double ALFA, double KP, double TB)
        {
            return Math.Round(Math.Log(ALFA) * (((1.84 - ALFA) * GM) + (23.69 - 5.3 * ALFA)) + ((6.078 * GM * Math.Pow(1.0048, (TB * KP - 273)) + (65.8 * Math.Pow(1.0047, (TB * KP - 273)))) * Math.Pow(10, -6)), 2);
        }
        public static double TB(double GM, double ALFA)
        {
            return Math.Round(2440 / Math.Pow(ALFA, 0.27) * Math.Exp((0.135 - 0.03 * ALFA) * GM));
        }
        public static double VALFA(double B, double M, double GM, double QNRM, double QNRG, double ALFA, double O2)
        {
            return Math.Round((B * M * GM * ((217.47 * ALFA - 52.3) * Math.Pow(O2, (-0.88 * Math.Pow(ALFA, 0.11))) * 29310)) / (3600 * QNRM) + ((B * M * (1 - GM) * (182 * ALFA - 56) * Math.Pow(O2, (-0.82 * Math.Pow(ALFA, 0.16))) / (3600 * QNRG))), 3);
        }
        public static double T(double KP, double TB)
        {
            return Math.Round(TB - 2 * (TB- (TB * KP)) / 3);
        }
        public static double NO(double VNO, double VALFA)
        {
            return Math.Round(VNO / (3600 * VALFA), 6);
        }
        public static double O2(double VK, double VKB, double VBB)
        {
            return Math.Round((VK + 0.2568 * (VBB + VKB)) / (VBB + VKB + VK) * 100, 2);
        }
    }
}
