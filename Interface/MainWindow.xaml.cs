﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Biblioteka;
using System.Data.SqlClient;
using Interface.Model;
using System.Diagnostics;
using Magnum.FileSystem;

namespace Interface
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Formula.B = Convert.ToDouble(B.Text);
                Formula.M = Convert.ToDouble(M.Text);
                Formula.GM = Convert.ToDouble(GM.Text);
                Formula.QNRM = Convert.ToDouble(QNRM.Text);
                Formula.QNRG = Convert.ToDouble(QNRG.Text);
                Formula.ALFA = Convert.ToDouble(ALFA.Text);
                Formula.KP = Convert.ToDouble(KP.Text);
                Formula.VK = Convert.ToDouble(VK.Text);
                Formula.VKB = Convert.ToDouble(VKB.Text);
                Formula.VBB = Convert.ToDouble(VBB.Text);
                Formula.NR = Convert.ToDouble(NR.Text);
                Formula.VKV = Convert.ToDouble(VKV.Text);
                Formula.N2K = Convert.ToDouble(N2K.Text);
                Formula.FG = Convert.ToDouble(FG.Text);
            }
            catch (FormatException)
            {
                MessageBox.Show("Введен неправильный формат числа", "Повторите попытку");
            }

            if (Formula.B == 0 || Formula.M == 0 || Formula.GM == 0 ||
                Formula.QNRM == 0 || Formula.QNRG == 0 || Formula.ALFA == 0
                || Formula.KP == 0 || Formula.VK == 0 || Formula.VKB == 0 ||
                Formula.VBB == 0 || Formula.NR == 0 || Formula.N2K == 0 || Formula.FG == 0)
            {
                MessageBox.Show("Значения не могут равняться нулю");
            }
            else
            {
                Formula.O2V = Formula.O2(Formula.VK, Formula.VKB, Formula.VBB);
                Formula.VALFAV = Formula.VALFA(Formula.B, Formula.M, Formula.GM, Formula.QNRM, Formula.QNRG, Formula.ALFA, Formula.O2V);
                Formula.TBV = Formula.TB(Formula.GM, Formula.ALFA);
                Formula.TV = Formula.T(Formula.KP, Formula.TBV);
                Formula.O2IV = Formula.O2I(Formula.GM, Formula.ALFA, Formula.KP, Formula.TBV);
                Formula.N2V = Formula.N2(Formula.O2V, Formula.ALFA, Formula.GM);
                Formula.NORV = Formula.NOR(Formula.KP, Formula.N2V, Formula.O2IV, Formula.TBV);
                Formula.NO1V = Formula.NO1(Formula.KP, Formula.NORV, Formula.TBV, Formula.TV);
                Formula.VNOV = Formula.VNO(Formula.B, Formula.M, Formula.GM, Formula.QNRM, Formula.NR, Formula.VKV, Formula.N2K, Formula.NO1V, Formula.VALFAV);
                Formula.MCNOV = Formula.MCNO(Formula.B, Formula.M, Formula.GM, Formula.QNRM, Formula.NR, Formula.VKV, Formula.N2K, Formula.NO1V, Formula.VALFAV);
                Formula.MYNOV = Formula.MYNO(Formula.B, Formula.M, Formula.GM, Formula.QNRM, Formula.NR, Formula.VKV, Formula.N2K, Formula.NO1V, Formula.VALFAV);
                Formula.MNOV = Formula.MNO(Formula.B, Formula.M, Formula.GM, Formula.QNRM, Formula.NR, Formula.VKV, Formula.N2K, Formula.NO1V, Formula.VALFAV);
                Formula.NOV = Formula.NO(Formula.VNOV, Formula.VALFAV);

                O2S.Text = Convert.ToString(Formula.O2V);
                TB.Text = Convert.ToString(Formula.TBV);
                T.Text = Convert.ToString(Formula.TV);
                O2I.Text = Convert.ToString(Formula.O2IV);
                N2.Text = Convert.ToString(Formula.N2V);
                NO1.Text = Convert.ToString(Formula.NO1V);
                NOR.Text = Convert.ToString(Formula.NORV);
                MCNO.Text = Convert.ToString(Formula.MCNOV);
                MYNO.Text = Convert.ToString(Formula.MYNOV);
                MNO.Text = Convert.ToString(Formula.MNOV);
                VNO.Text = Convert.ToString(Formula.VNOV);
                NO.Text = Convert.ToString(Formula.NOV);
                VALFA.Text = Convert.ToString(Formula.VALFAV);

                O2S.Visibility = Visibility.Visible;
                TB.Visibility = Visibility.Visible;
                T.Visibility = Visibility.Visible;
                O2I.Visibility = Visibility.Visible;
                N2.Visibility = Visibility.Visible;
                NO1.Visibility = Visibility.Visible;
                NOR.Visibility = Visibility.Visible;
                MCNO.Visibility = Visibility.Visible;
                MYNO.Visibility = Visibility.Visible;
                MNO.Visibility = Visibility.Visible;
                VNO.Visibility = Visibility.Visible;
                NO.Visibility = Visibility.Visible;
                VALFA.Visibility = Visibility.Visible;
                bReport.Visibility = Visibility.Visible;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            B.Text="133,7";
            M.Text="42,85";
            GM.Text = "0,205";
            QNRM.Text = "40190";
            QNRG.Text = "33410";
            ALFA.Text = "1,345";
            KP.Text = "0,81";
            VK.Text = "1500";
            VKB.Text = "3500";
            VBB.Text = "47187";
            NR.Text = "0,5";
            VKV.Text = "0";
            N2K.Text = "2,85";
            FG.Text = "107,4";

            O2S.Text = "";
            TB.Text = "";
            T.Text = "";
            O2I.Text = "";
            N2.Text = "";
            NO1.Text = "";
            NOR.Text = "";
            MCNO.Text = "";
            MYNO.Text = "";
            MNO.Text = "";
            VNO.Text = "";
            NO.Text = "";
            VALFA.Text = "";

            O2S.Visibility = Visibility.Hidden;
            TB.Visibility = Visibility.Hidden;
            T.Visibility = Visibility.Hidden;
            O2I.Visibility = Visibility.Hidden;
            N2.Visibility = Visibility.Hidden;
            NO1.Visibility = Visibility.Hidden;
            NOR.Visibility = Visibility.Hidden;
            MCNO.Visibility = Visibility.Hidden;
            MYNO.Visibility = Visibility.Hidden;
            MNO.Visibility = Visibility.Hidden;
            VNO.Visibility = Visibility.Hidden;
            NO.Visibility = Visibility.Hidden;
            VALFA.Visibility = Visibility.Hidden;
            bReport.Visibility = Visibility.Hidden;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            B.Text = "";
            M.Text = "";
            GM.Text = "";
            QNRM.Text = "";
            QNRG.Text = "";
            ALFA.Text = "";
            KP.Text = "";
            VK.Text = "";
            VKB.Text = "";
            VBB.Text = "";
            NR.Text = "";
            VKV.Text = "";
            N2K.Text = "";
            FG.Text = "";

            O2S.Text = "";
            TB.Text = "";
            T.Text = "";
            O2I.Text = "";
            N2.Text = "";
            NO1.Text = "";
            NOR.Text = "";
            MCNO.Text = "";
            MYNO.Text = "";
            MNO.Text = "";
            VNO.Text = "";
            NO.Text = "";
            VALFA.Text = "";

            O2S.Visibility = Visibility.Hidden;
            TB.Visibility = Visibility.Hidden;
            T.Visibility = Visibility.Hidden;
            O2I.Visibility = Visibility.Hidden;
            N2.Visibility = Visibility.Hidden;
            NO1.Visibility = Visibility.Hidden;
            NOR.Visibility = Visibility.Hidden;
            MCNO.Visibility = Visibility.Hidden;
            MYNO.Visibility = Visibility.Hidden;
            MNO.Visibility = Visibility.Hidden;
            VNO.Visibility = Visibility.Hidden;
            NO.Visibility = Visibility.Hidden;
            VALFA.Visibility = Visibility.Hidden;
            bReport.Visibility = Visibility.Hidden;
        }

        private void bReport_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection connection = new SqlConnection(Properties.Settings.Default.TRPO_KursovayaConnectionString);
            connection.Open();
            DataClasses2DataContext db = new DataClasses2DataContext();
            SqlCommand commando2s = new SqlCommand("Update [Table_1] Set [Znachenie]=@o2s where [Id]=1", connection);
            SqlCommand commandtb = new SqlCommand("Update [Table_1] Set [Znachenie]=@tb where [Id]=2", connection);
            SqlCommand commandt = new SqlCommand("Update [Table_1] Set [Znachenie]=@t where [Id]=3", connection);
            SqlCommand commando2i = new SqlCommand("Update [Table_1] Set [Znachenie]=@o2i where [Id]=4", connection);
            SqlCommand commandn2 = new SqlCommand("Update [Table_1] Set [Znachenie]=@n2 where [Id]=5", connection);
            SqlCommand commandnor = new SqlCommand("Update [Table_1] Set [Znachenie]=@nor where [Id]=6", connection);
            SqlCommand commandno1 = new SqlCommand("Update [Table_1] Set [Znachenie]=@no1 where [Id]=7", connection);
            SqlCommand commandmcno = new SqlCommand("Update [Table_1] Set [Znachenie]=@mcno where [Id]=8", connection);
            SqlCommand commandmyno = new SqlCommand("Update [Table_1] Set [Znachenie]=@myno where [Id]=9", connection);
            SqlCommand commandmno = new SqlCommand("Update [Table_1] Set [Znachenie]=@mno where [Id]=10", connection);
            SqlCommand commandvno = new SqlCommand("Update [Table_1] Set [Znachenie]=@vno where [Id]=11", connection);
            SqlCommand commandno = new SqlCommand("Update [Table_1] Set [Znachenie]=@no where [Id]=12", connection);
            SqlCommand commandvalfa = new SqlCommand("Update [Table_1] Set [Znachenie]=@valfa where [Id]=13", connection);

            commando2s.Parameters.AddWithValue("o2s", O2S.Text);
            commandtb.Parameters.AddWithValue("tb", TB.Text);
            commandt.Parameters.AddWithValue("t", T.Text);
            commando2i.Parameters.AddWithValue("o2i", O2I.Text);
            commandn2.Parameters.AddWithValue("n2", N2.Text);
            commandnor.Parameters.AddWithValue("nor", NOR.Text);
            commandno1.Parameters.AddWithValue("no1", NO1.Text);
            commandmcno.Parameters.AddWithValue("mcno", MCNO.Text);
            commandmyno.Parameters.AddWithValue("myno", MYNO.Text);
            commandmno.Parameters.AddWithValue("mno", MNO.Text);
            commandvno.Parameters.AddWithValue("vno", VNO.Text);
            commandno.Parameters.AddWithValue("no", NO.Text);
            commandvalfa.Parameters.AddWithValue("valfa", VALFA.Text);

            commando2s.ExecuteNonQuery();
            commandtb.ExecuteNonQuery();
            commandt.ExecuteNonQuery();
            commando2i.ExecuteNonQuery();
            commandn2.ExecuteNonQuery();
            commandnor.ExecuteNonQuery();
            commandno1.ExecuteNonQuery();
            commandmcno.ExecuteNonQuery();
            commandmyno.ExecuteNonQuery();
            commandmno.ExecuteNonQuery();
            commandvno.ExecuteNonQuery();
            commandno.ExecuteNonQuery();
            commandvalfa.ExecuteNonQuery();

            var s = new ReportForm();
            s.ShowDialog();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Process process = new Process();
            process.StartInfo = new ProcessStartInfo()
            {
                FileName = "Справочник.pdf"
            };
            process.Start();
        }
    }
}
