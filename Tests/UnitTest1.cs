using NUnit.Framework;
using Biblioteka;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }
        
        [Test]
        public void VNO()
        {
            double B = 133.7;
            double M = 42.85;
            double GM = 0.205;
            double QNRM = 40190;
            double NR = 0.5;
            double VKV = 0;
            double N2K=2.85;
            double NO1 = 0.00017;
            double VALFA = 2.779;
            double result = Formula.VNO(B,M,GM,QNRM,NR,VKV,N2K,NO1,VALFA);
            Assert.AreEqual(6.845, result);
        }

        [Test]
        public void MNO()
        {
            double B = 133.7;
            double M = 42.85;
            double GM = 0.205;
            double QNRM = 40190;
            double NR = 0.5;
            double VKV = 0;
            double N2K = 2.85;
            double NO1 = 0.00017;
            double VALFA = 2.779;
            double result = Formula.MNO(B, M, GM, QNRM, NR, VKV, N2K, NO1, VALFA);
            Assert.AreEqual(9.174, result);
        }

        [Test]
        public void MYNO()
        {
            double B = 133.7;
            double M = 42.85;
            double GM = 0.205;
            double QNRM = 40190;
            double NR = 0.5;
            double VKV = 0;
            double N2K = 2.85;
            double NO1 = 0.00017;
            double VALFA = 2.779;
            double result = Formula.MYNO(B, M, GM, QNRM, NR, VKV, N2K, NO1, VALFA);
            Assert.AreEqual(0.214, result);
        }
        
        [Test]
        public void MCNO()
        {
            double B = 133.7;
            double M = 42.85;
            double GM = 0.205;
            double QNRM = 40190;
            double NR = 0.5;
            double VKV = 0;
            double N2K = 2.85;
            double NO1 = 0.00017;
            double VALFA = 2.779;
            double result = Formula.MCNO(B, M, GM, QNRM, NR, VKV, N2K, NO1, VALFA);
            Assert.AreEqual(0.000919, result);
        }

        [Test]
        public void NOR()
        {
            double KP = 0.81;
            double N2 = 64.27;
            double O2I = 5.05;
            double TB = 2296;
            double result = Formula.NOR(KP, N2, O2I, TB);
            Assert.AreEqual(0.00017, result);
        }

        [Test]
        public void NO1()
        {
            double KP = 0.81;
            double NOR = 0.00017;
            double TB = 2296;
            double T = 2005;
            double result = Formula.NO1(KP, NOR, TB, T);
            Assert.AreEqual(0.00017,result);
        }

        [Test]
        public void N2()
        {
            double O2 = 27.82;
            double ALFA = 1.345;
            double GM = 0.205;
            double result = Formula.N2(O2, ALFA, GM);
            Assert.AreEqual(64.27,0.1,result);
        }

        [Test]
        public void O2I()
        {
            double GM = 0.205;
            double ALFA = 1.345;
            double KP = 0.81;
            double TB = 2296;
            double result = Formula.O2I(GM, ALFA, KP, TB);
            Assert.AreEqual(5.05,result);
        }
        
        [Test]
        public void TB()
        {
            double GM = 0.205;
            double ALFA = 1.345;
            double result = Formula.TB(GM, ALFA);
            Assert.AreEqual(2296, result);
        }

        [Test]
        public void VALFA()
        {
            double B = 133.7;
            double M = 42.85;
            double GM = 0.205;
            double QNRM = 40190;
            double QNRG = 33410;
            double ALFA = 1.345;
            double O2 = 27.82;
            double result = Formula.VALFA(B, M, GM, QNRM, QNRG, ALFA, O2);
            Assert.AreEqual(2.779, result);
        }

        [Test]
        public void T()
        {
            double KP = 0.81;
            double TB = 2296;
            double result = Formula.T(KP, TB);
            Assert.AreEqual(2005, result);
        }

        [Test]
        public void NO()
        {
            double VNO = 6.845;
            double VALFA = 2.779;
            double result = Formula.NO(VNO, VALFA);
            Assert.AreEqual(0.000684, result, 0.00001);
        }

        [Test]
        public void O2()
        {
            double VK = 1500;
            double VKB = 3500;
            double VBB = 47187;
            double result = Formula.O2(VK, VKB, VBB);
            Assert.AreEqual(27.82, result);
        }
    }
}